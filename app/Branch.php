<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table='branches';
    public function Warehouse(){
        return $this->belongsToMany(Warehouse::class,'branch_warehouses','branch_id','warehouse_id');
    }
}
