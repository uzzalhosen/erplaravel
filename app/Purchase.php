<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function Vendor(){
        return $this->belongsTo(Vendor::class);
    }
    public function Branch(){
        return $this->belongsTo(Branch::class);
    }
    public function Details(){
        return $this->hasMany(PurchaseDetail::class);
    }
}
