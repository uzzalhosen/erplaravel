<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class saleDetail extends Model
{
    public function Sale(){
        return $this->belongsTo(sale::class);
    }
    public function Product(){
        return $this->belongsTo(Product::class);
    }
}
