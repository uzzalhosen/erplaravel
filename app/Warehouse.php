<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    public function Branches(){
        return $this->belongsToMany(Branch::class,'branch_warehouses','warehouse_id','branch_id');
    }
}
