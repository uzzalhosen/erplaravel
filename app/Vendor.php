<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    public function Type(){
        return $this->belongsTo(VendorType::class,'vendor_type_id');
    }
}
