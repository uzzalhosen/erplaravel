<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function Details(){
        return $this->hasMany(ProductDetail::class);
    }
    public function Category(){
        return $this->belongsTo(Category::class);
    }
    public function ProductType(){
        return $this->belongsTo(ProductType::class);
    }
//    public function UnitOfMeasure(){
//        return $this->belongsTo(UnitOfMeasure::class);
//    }
    public function Branch(){
        return $this->belongsTo(Branch::class);
    }
    public function Currency(){
        return $this->belongsTo(Currency::class);
    }
    public function Warehouse(){
        return $this->belongsTo(Warehouse::class);
    }




//    public function Category(){
//        return $this->belongsTo(Category::class,'category_id');
//    }
    public function UnitOfMeasure(){
        return $this->belongsTo(UnitOfMeasure::class,'unit_of_measure_id');
    }
//    public function Branch(){
//        return $this->belongsTo(Branch::class,'branch_id');
//    }
//    public function Currency(){
//        return $this->belongsTo(Currency::class,'currency_id');
//    }
//    public function Warehouse(){
//        return $this->belongsTo(Warehouse::class,'warehouse_id');
//    }
}
