<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    public function Purchase(){
        return $this->belongsTo(Purchase::class);
    }
    public function Product(){
        return $this->belongsTo(Product::class);
    }
}
