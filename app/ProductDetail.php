<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    public function Product(){
        return $this->belongsTo(Product::class);
    }
    public function Branch(){
        return $this->belongsTo(Branch::class);
    }
    public function Warehouse(){
        return $this->belongsTo(Warehouse::class);
    }
}
