<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sale extends Model
{
    public function Customer(){
        return $this->belongsTo(customer::class);
    }
    public function Branch(){
        return $this->belongsTo(Branch::class);
    }
    public function Details(){
        return $this->hasMany(saleDetail::class);
    }
}
