<?php

namespace App\Http\Controllers;

use App\customerType;
use App\Http\Resources\CustomerTypeResource;
use Illuminate\Http\Request;

class CustomerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Fetched Successfully",
            "data" => [
                "customerType" => CustomerTypeResource::collection(customerType::paginate(20))
            ],
        ];
//        return CustomerTypeResource::collection(customerType::paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type  = new customerType();
        $type->name= $request->name;
        $type->description= $request->description;
        $type->save();
        return $type;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\customerType  $customerType
     * @return \Illuminate\Http\Response
     */
    public function show(customerType $customerType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\customerType  $customerType
     * @return \Illuminate\Http\Response
     */
    public function edit(customerType $customerType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\customerType  $customerType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $type  = customerType::find($id);
        $type->name= $request->name;
        $type->description= $request->description;
        $type->save();
        return $type;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\customerType  $customerType
     * @return \Illuminate\Http\Response
     */
    public function destroy(customerType $customerType)
    {
        //
    }
}
