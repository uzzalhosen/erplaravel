<?php

namespace App\Http\Controllers;

use App\Http\Resources\VendorTypeResource;
use App\VendorType;
use Illuminate\Http\Request;

class VendorTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return VendorTypeResource::collection(VendorType::paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type  = new VendorType();
        $type->name= $request->name;
        $type->description= $request->description;
        $type->save();
        return $type;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VendorType  $vendorType
     * @return \Illuminate\Http\Response
     */
    public function show(VendorType $vendorType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VendorType  $vendorType
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorType $vendorType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VendorType  $vendorType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type  = VendorType::find($id);
        $type->name= $request->name;
        $type->description= $request->description;
        $type->save();
        return $type;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VendorType  $vendorType
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorType $vendorType)
    {
        //
    }
}
