<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Http\Resources\Resource;
use App\Http\Resources\SaleResource;
use App\Product;
use App\ProductDetail;
use App\sale;
use App\saleDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllSales()
    {
        $sales = sale::with('Details.Product','Customer','Branch')->orderByDesc('id')->get();

        return Resource::collection($sales)
            ->additional(['total' => [
            'amount' => $sales->sum('cart_total'),
            'vat' =>  $sales->sum('tax'),
            'discount' =>  $sales->sum('discount_amount'),
            'total' =>  $sales->sum('grand_total'),
            ],
            'branches'=>Branch::all()
            ]);
    }
    public function index(Request $request)
    {
        $sales = sale::with('Details.Product','Customer','Branch')->where('status','!=','Due')->orderByDesc('id')->paginate(20);
        $stat = sale::with('Details.Product','Customer','Branch')->where('status','!=','Due')->orderByDesc('id')->get();

        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Fetched Successfully",
            "data" => [
                "sales" => $sales,
                "total"=> [
                    'amount' => $stat->sum('cart_total'),
                    'vat' =>  $stat->sum('tax'),
                    'discount' =>  $stat->sum('discount_amount'),
                    'total' =>  $stat->sum('grand_total'),
                ],
                'branches'=>Branch::all()
            ],
        ];
    }
    public function dueSales(Request $request)
    {
        $sales = sale::with('Details.Product','Customer','Branch')->where('status','Due')->orderByDesc('id')->paginate(20);
        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Fetched Successfully",
            "data" => [
                "sales" => $sales,
                "total"=> [
                    'amount' => $sales->sum('cart_total'),
                    'vat' =>  $sales->sum('tax'),
                    'discount' =>  $sales->sum('discount_amount'),
                    'total' =>  $sales->sum('grand_total'),
                ],
                'branches'=>Branch::all()
            ],
        ];
    }
    public function changeAction(Request $request){
//        return $request->date;
        $sales = sale::find($request->id);
        if ($request->status=='Accepted'){
            foreach ($sales->Details as $detail){
                $sales->date = $request->date;
                $product = Product::find($detail->product_id);
                $product->available_qty = $product->available_qty-$detail->qty;
                $product->save();
            }
        }
        elseif ($request->status=='Rejected'){
            foreach ($sales->Details as $detail){
                $sales->date = $request->date;
                $product = Product::find($detail->product_id);
                $product->available_qty = $product->available_qty+$detail->qty;
                $product->save();
            }
        }
        $sales->status = $request->status;
        if ($sales->save()){
            return [
                'success'=>true
            ];
        }
    }
    public function filterByBranch($branch)
    {
        $sales = sale::with('Details.Product','Customer','Branch')->where('branch_id',$branch)->orderByDesc('id')->paginate(20);
        return Resource::collection($sales)
            ->additional(['total' => [
                'amount' => sale::all()->where('branch_id',$branch)->sum('cart_total'),
                'vat' =>  sale::all()->where('branch_id',$branch)->sum('tax'),
                'discount' =>  sale::all()->where('branch_id',$branch)->sum('discount_amount'),
                'total' =>  sale::all()->where('branch_id',$branch)->sum('grand_total'),
            ],
                'branches'=>Branch::all()
            ]);
    }
    public function getFilterBy(Request $request)
    {
        if ($request->isDue==true){
//            return 'true';
            if ($request->filterDate != null){
                $sales = sale::with('Details.Product','Customer','Branch')
                    ->whereBetween('date',[$request->filterDate['start_date'],$request->filterDate['end_date']])
                    ->where($request->column,$request->data)
                    ->where('status','Due')->orderByDesc('id')->paginate(20);
                $stat = sale::with('Details.Product','Customer','Branch')
                    ->whereBetween('date',[$request->filterDate['start_date'],$request->filterDate['end_date']])
                    ->where($request->column,$request->data)
                    ->where('status','Due')->orderByDesc('id')->get();
            }else{
                $sales = sale::with('Details.Product','Customer','Branch')
                    ->where($request->column,$request->data)
                    ->where('status','Due')->orderByDesc('id')->paginate(20);
                $stat = sale::with('Details.Product','Customer','Branch')
                    ->where($request->column,$request->data)
                    ->where('status','Due')->orderByDesc('id')->get();
            }

        }else if ($request->isDue == false){
//            return $request->filterDate['start_date'];
            if ($request->filterDate != null){

                $sales = sale::with('Details.Product','Customer','Branch')
                    ->whereBetween('date',[$request->filterDate['start_date'],$request->filterDate['end_date']])
                    ->where($request->column,$request->data)
                    ->where('status','!=','Due')->orderByDesc('id')->paginate(20);
                $stat = sale::with('Details.Product','Customer','Branch')
                    ->whereBetween('date',[$request->filterDate['start_date'],$request->filterDate['end_date']])
                    ->where($request->column,$request->data)
                    ->where('status','!=','Due')->orderByDesc('id')->get();
            }else{

                $sales = sale::with('Details.Product','Customer','Branch')
                    ->where($request->column,$request->data)
                    ->where('status','!=','Due')->orderByDesc('id')->paginate(20);
                $stat = sale::with('Details.Product','Customer','Branch')
                    ->where($request->column,$request->data)
                    ->where('status','!=','Due')->orderByDesc('id')->get();
            }
        }
/*
 *
 * Only for Mobile
 * Check whether Platform is mobile
 *
 * */
        if ($request->hasHeader('platform')){
            $platform = $request->header('platform');
            if ($platform == 'mobile'){
                return [
                    "code" => "200",
                    "status" => "success",
                    "message" => "Item Fetched Successfully",
                    "data" => [
                        'salesData' => [
                            "sales" => $sales,
                            "total"=> [
                                'amount' => $stat->sum('cart_total'),
                                'vat' =>  $stat->sum('tax'),
                                'discount' =>  $stat->sum('discount_amount'),
                                'total' =>  $stat->sum('grand_total'),
                            ],
                            'branches'=>Branch::all()
                        ]
                    ],
                ];
            }
        }
        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Fetched Successfully",
            "data" => [
                "sales" => $sales,
                "total"=> [
                    'amount' => $stat->sum('cart_total'),
                    'vat' =>  $stat->sum('tax'),
                    'discount' =>  $stat->sum('discount_amount'),
                    'total' =>  $stat->sum('grand_total'),
                ],
                'branches'=>Branch::all()
            ],
        ];
//        return Resource::collection($sales)
//            ->additional(['total' => [
//                'amount' => $sales->sum('cart_total'),
//                'vat' =>  $sales->sum('tax'),
//                'discount' =>  $sales->sum('discount_amount'),
//                'total' =>  $sales->sum('grand_total'),
//            ],
//                'branches'=>Branch::all()
//            ]);
    }
    public function getFilterBy1($column,$data,$filterDate,$isDue=false)
    {
        if ($isDue=='true'){
//            return 'true';
            $sales = sale::with('Details.Product','Customer','Branch')->whereBetween('date',[$filterDate->start_date,$filterDate->end_date])->where($column,$data)->where('status','Due')->orderByDesc('id')->paginate(20);
        }else if ($isDue == 'false'){
//            return 'false';
            $sales = sale::with('Details.Product','Customer','Branch')->whereBetween('date',[$filterDate->start_date,$filterDate->end_date])->where($column,$data)->where('status','!=','Due')->orderByDesc('id')->paginate(20);
        }
        return Resource::collection($sales)
            ->additional(['total' => [
                'amount' => $sales->sum('cart_total'),
                'vat' =>  $sales->sum('tax'),
                'discount' =>  $sales->sum('discount_amount'),
                'total' =>  $sales->sum('grand_total'),
            ],
                'branches'=>Branch::all()
            ]);
    }

    public function searchSalesByDate($from,$to,$isDue=false)
    {
        if ($isDue=='true'){
            $sales = sale::with('Details.Product','Customer','Branch')->whereBetween('date',[$from,$to])->where('status','Due')->orderByDesc('id')->paginate(20);
            $stat = sale::with('Details.Product','Customer','Branch')->whereBetween('date',[$from,$to])->where('status','Due')->orderByDesc('id')->get();
        }else {
            $sales = sale::with('Details.Product','Customer','Branch')->whereBetween('date',[$from,$to])->where('status','!=','Due')->orderByDesc('id')->paginate(20);
            $stat = sale::with('Details.Product','Customer','Branch')->whereBetween('date',[$from,$to])->where('status','!=','Due')->orderByDesc('id')->get();
        }
        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Fetched Successfully",
            "data" => [
                "sales" => $sales,
                "total"=> [
                    'amount' => $stat->sum('cart_total'),
                    'vat' =>  $stat->sum('tax'),
                    'discount' =>  $stat->sum('discount_amount'),
                    'total' =>  $stat->sum('grand_total'),
                ],
                'branches'=>Branch::all()
            ],
        ];
    }
    public function searchSalesByDateDue($from,$to)
    {
        $sales = sale::with('Details.Product','Customer','Branch')->whereBetween('date',[$from,$to])->where('status','=','Due')->orderByDesc('id')->paginate(20);
        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Fetched Successfully",
            "data" => [
                "sales" => $sales,
                "total"=> [
                    'amount' => $sales->sum('cart_total'),
                    'vat' =>  $sales->sum('tax'),
                    'discount' =>  $sales->sum('discount_amount'),
                    'total' =>  $sales->sum('grand_total'),
                ],
                'branches'=>Branch::all()
            ],
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function receive(Request $request)
    {
        $sale                   =           sale::find($request->id);
        if ($sale->due_amount=='0.00'){
//            return $sale->grand_total-$request->amount;
            $sale->due_amount       =           $sale->grand_total-$request->amount;
        }else{
//            return 'not 0.00';
            $sale->due_amount       =           $sale->due_amount-$request->amount;
        }
        if ($sale->grand_total<=($sale->paid_amount+$request->amount)){
//            return $sale->paid_amount;
            $sale->status ='Paid';
        }else{
//            return 'grant total '.$sale->grand_total;
            $sale->status ='Paid';
        }
//        if ($sale->grand_total===($sale->due_amount+$request->amount)){
//            $sale->status ='Paid';
//        }else{
//            $sale->status ='Partial Paid';
//        }
        $sale->paid_amount      =           $sale->paid_amount+$request->amount;
        $sale->grand_total      =           $request->amount;
        $sale->discount_amount      =           $request->discount_amount;
        $sale->cart_total      =           $request->cart_total;
        $sale->save();
        foreach ($request->details as $item => $value){
            $saleDetail = saleDetail::find($request->details[$item]['id']);
            $saleDetail->sub_total = $request->details[$item]['receiveAmount'];
            $saleDetail->total_received = $request->details[$item]['receive'];
            $saleDetail->return_qty = $request->details[$item]['return_qty'];
            $saleDetail->save();
        }


    }
    public function deliverProducts(Request $request)
    {
       $results = json_decode($request->getContent(), true);
//       for ($i=1;$i<sizeof($results);$i++){
//           $id []=$results[$i]['id'];
//           $receive []=$results['receive'];
//       }
        // $receive = array();
        // foreach ($results as $result){
        //    if (isset($result['receive'])){

        //        $receive []=$result['receive'];
        //        $detail = saleDetail::find($result['id']);


        //        $product = Product::find($detail->product_id);
        //        $product->actual_qty = $product->actual_qty-$result['receive'];
        //        $product->save();

        //        $detail->total_received = $detail->total_received+$result['receive'];
        //        $detail->save();
        //    }
        // }
        // return $receive;
        foreach($results['items'] as $result) {

            if (!isset($result['receive'])){
                return 'failed';
            }

            $productDetails = ProductDetail::where('product_id', $result['product_id'])
            ->where('warehouse_id', $request->extra['warehouse_id'])
            ->where('branch_id', $request->extra['branch_id'])
            ->orderBy('expire_date', 'ASC')
            ->get();

            $total = collect($productDetails)->sum('qty');
            $sum = 0;
            $temp = 0;

            if($total >= $result['receive']) {
                foreach($productDetails as $productDetail) {
                    $sum = $sum + $productDetail->qty;
                    if($sum >= $result['receive']) {
                        $productDetail->qty = $productDetail->qty + $temp - $result['receive'];
                        $productDetail->save();
                        break;
                    } else {
                        $temp = $temp + $productDetail->qty;
                        $productDetail->qty = 0;
                        $productDetail->save();
                    }
                }
                $product = Product::find($result['product_id']);
//                $product->available_qty = $product->available_qty - $result['receive'];
                $product->actual_qty = $product->actual_qty - $result['receive'];
                $product->save();

                $saleDetail = saleDetail::find($result['id']);
                $saleDetail->total_received =$result['receive'];
                $saleDetail->return_qty = $result['return_qty'];
                $saleDetail->save();
                $sale = sale::find($saleDetail->sale_id);
                $sale->status = 'Delivered';
                $sale->save();
            } else {

            }

       }
    }

    public function TotalStock(Request $request) {
        $productDetail = ProductDetail::where('branch_id', $request->branchID)
        ->where('product_id', $request->productID)
        ->get();
        $total = collect($productDetail)->sum('qty');
        return [
            'totalStock'=> $total
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        return redirect($_SERVER['HTTP_REFERER']);
        //        return Product::whereIn('id',collect($request->products)->pluck('product_id'))->get()->sum('buying_price');
        $sale = new sale();
        $sale->customer_id = $request->customer_id;
        $sale->date = date('Y-m-d');
        $sale->status = 'Pending';
        if (isset($request->salesman_id)) {

            $sale->salesman_id = $request->salesman_id;
        } else {

            $sale->salesman_id = 1;
        }
        $sale->branch_id = $request->branch_id;
        $sale->cart_total = $request->subtotal;
        $sale->YourReference = $request->YourReference;
        $sale->OurReference = $request->OurReference;
        $sale->discount_amount = $request->discount_amount;
        $sale->discount = $request->discount;
        $sale->discount_type = $request->discount_type;
        $sale->tax = $request->tax;
        $sale->tax_amount = $request->tax_amount;
        $sale->vat = '0';
        if (isset($request->grand_total)) {
            $sale->grand_total = $request->grand_total;
        }
        $sale->buying_total = Product::whereIn('id', collect($request->products)->pluck('product_id'))->get()->sum('buying_price');
        $sale->paid_amount = '0';
        $sale->due_amount = '0';
        $sale->save();
        $buying_total_sum= 0;
        foreach ($request->products as $item => $value) {

            //            $product = Product::find($request->products[$item]['product_id']);
            //            $product->available_qty = $product->available_qty-$request->products[$item]['quantity'];
            //            $product->save();

            /**
             * Insert Sales Details
             *
             */
            $saleDetail = new saleDetail();
            $saleDetail->sale_id = $sale->id;
            $saleDetail->product_id = $request->products[$item]['product_id'];
            $saleDetail->qty = $request->products[$item]['quantity'];
            $saleDetail->total_received = 0;
            $saleDetail->return_qty = 0;
            $saleDetail->unit_price = $request->products[$item]['price'];
            $saleDetail->sub_total = $request->products[$item]['amount'];
            $buying_total_sum = $buying_total_sum + $request->products[$item]['quantity']*Product::find($request->products[$item]['product_id'])->buying_price;
            $saleDetail->save();


            /**
             *
             * Handling product Lot
             *
             *
             */
            // $productDetails = ProductDetail::where('product_id', $request->products[$item]['product_id'])
            //     ->orderBy('expire_date', 'ASC')
            //     ->get();

            // $total = collect($productDetails)->sum('qty');
            // $sum = 0;
            // $temp = 0;
            // if($total >= $request->products[$item]['quantity']) {
            //     foreach($productDetails as $productDetail) {
            //         $sum = $sum + $productDetail->qty;
            //         if($sum >= $request->products[$item]['quantity']) {
            //             $productDetail->qty = $productDetail->qty + $temp - $request->products[$item]['quantity'];
            //             $productDetail->save();
            //             break;
            //         } else {
            //             $temp = $temp + $productDetail->qty;
            //             $productDetail->qty = 0;
            //             $productDetail->save();
            //         }
            //     }
            // } else {

            // }

            // $product = Product::find($request->products[$item]['product_id']);
            // $product->available_qty = $product->available_qty - $request->products[$item]['quantity'];
            // $product->actual_qty = $product->actual_qty - $request->products[$item]['quantity'];
            // $product->save();


        }
        $sale->buying_total = $buying_total_sum;
        $sale->save();
        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Saved Successfully",
            "data" => [
                "sale" => $sale
            ],
        ];
    }
    public function store1(Request $request)
    {
//        return redirect($_SERVER['HTTP_REFERER']);
//        return Product::whereIn('id',collect($request->products)->pluck('product_id'))->get()->sum('buying_price');
        $sale = new sale();
        $sale->customer_id = $request->customer_id;
        $sale->date = date('Y-m-d');
        $sale->status = 'Pending';
        if (isset($request->salesman_id)) {

            $sale->salesman_id = $request->salesman_id;
        } else {

            $sale->salesman_id = 1;
        }
        $sale->branch_id = $request->branch_id;
        $sale->cart_total = $request->subtotal;
        $sale->YourReference = $request->YourReference;
        $sale->OurReference = $request->OurReference;
        $sale->discount_amount = $request->discount_amount;
        $sale->discount = $request->discount;
        $sale->discount_type = $request->discount_type;
        $sale->tax = $request->tax;
        $sale->tax_amount = $request->tax_amount;
        $sale->vat = '0';
        if (isset($request->grand_total)) {
            $sale->grand_total = $request->grand_total;
        }
        $sale->buying_total = Product::whereIn('id', collect($request->products)->pluck('product_id'))->get()->sum('buying_price');
        $sale->paid_amount = '0';
        $sale->due_amount = '0';
        $sale->save();
        $buying_total_sum= 0;
        foreach ($request->products as $item => $value) {

//            $product = Product::find($request->products[$item]['product_id']);
//            $product->available_qty = $product->available_qty-$request->products[$item]['quantity'];
//            $product->save();

            $saleDetail = new saleDetail();
            $saleDetail->sale_id = $sale->id;
            $saleDetail->product_id = $request->products[$item]['product_id'];
            $saleDetail->qty = $request->products[$item]['quantity'];
            $saleDetail->total_received = 0;
            $saleDetail->return_qty = 0;
            $saleDetail->unit_price = $request->products[$item]['price'];
            $saleDetail->sub_total = $request->products[$item]['amount'];
            $buying_total_sum = $buying_total_sum + $request->products[$item]['quantity']*Product::find($request->products[$item]['product_id'])->buying_price;
            $saleDetail->save();
        }
        $sale->buying_total = $buying_total_sum;
        $sale->save();
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new Resource(sale::with('Customer','Details','Details.Product','Branch')->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(sale $sale)
    {
        //
    }

    public function salesManReport(Request $request)
    {
        $sales = sale::with('Customer')
            ->where('salesman_id',$request->employee_id)
            ->whereBetween('date',[$request->fromDate,$request->toDate])
            ->orderBy('date', 'desc')
            ->get()
            ->groupBy(function($val) {
                return Carbon::parse($val->date)->format('F');
            });
        $summary = $sales->map(function ($sale){
            return [
                'cart_total' => number_format(collect($sale)->sum('cart_total'), 2, '.', ''),
                'discount' => number_format(collect($sale)->sum('discount_amount'), 2, '.', ''),
                'total' => number_format(collect($sale)->sum('grand_total'), 2, '.', '')
            ];
        });
        return [
            'code'=>200,
            'status'=>'success',
            'message'=>'',
            'data'=> [
                'reports' => $sales,
                'summary' => $summary
            ]
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(sale $sale)
    {
        //
    }


    public function deleteSale($id){
        $sale = sale::find($id);
        if (collect($sale)->isNotEmpty()){
            if ($sale->status!='Pending'){
                foreach ($sale->Details as $detail){
                    $product = Product::find($detail->product_id);
                    $product->available_qty = $product->available_qty+$detail->qty;
                    $product->save();
                }
            }
            $saleDetail = saleDetail::where('sale_id',$id)->get();
            if (collect($saleDetail)->isNotEmpty()){
                foreach ($saleDetail as $detail){
                    $detail->delete();
                }
            }
            $sale->delete();
        }

    }
}
