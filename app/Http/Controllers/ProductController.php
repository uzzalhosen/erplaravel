<?php

namespace App\Http\Controllers;

use App\Http\Resources\DefaultResouce;
use App\Http\Resources\ProductAllResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\Resource;
use App\Product;
use App\ProductDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function index(Request $request)
    {
        $products = Product::where('available_qty','>',0)->get();
        $productMeta = Product::all();
        $productMeta = $productMeta->map(function ($pr){
            $pr['total_buying_price'] =$pr['buying_price']*$pr['available_qty'];
            $pr['total_selling_price'] =$pr['selling_price']*$pr['available_qty'];
            $pr['total_mrp'] =$pr['mrp']*$pr['available_qty'];
            return $pr;
        });
//        return $productMeta;
//        dd($productMeta);

//


        if (isset($request->stock) && $request->stock==0){
            return [
                "code" => "200",
                "status" => "success",
                "message" => "Item Fetched Successfully",
                "data" => [
                    'product'=>ProductAllResource::collection(Product::all())
                ]
            ];
        }
        elseif (isset($request->stock) && $request->stock==1){
            return [
                "code" => "200",
                "status" => "success",
                "message" => "Item Fetched Successfully",
                "data" => [
                    'product'=>ProductAllResource::collection($products)
                ]
            ];
        }else{
            if (isset($request->search) && $request->search != 'undefined'){
                $pp = Product::where('name','like', '%' . $request->search . '%')->paginate(20);
            }else{
                $pp = Product::paginate(20);
            }

            $prod = new ProductResource($pp);
            return [
                "code" => "200",
                "status" => "success",
                "message" => "Item Fetched Successfully",
                'data'=>$prod,
                "extra" => [
                    'total_buying_price' => $productMeta->sum('total_buying_price'),
                    'total_selling_price' => $productMeta->sum('total_selling_price'),
                    'total_mrp' => $productMeta->sum('total_mrp')
                ]
            ];
        }
    }
    public function allNotificationByQty(){
//        return Carbon::now()->addMonth(6);
        return Resource::collection(Product::with('Details','Details.Product','Details.Branch','Details.Warehouse')->whereColumn('re_order_level','>=','available_qty')->get());
//        return Resource::collection(Product::with('Details','Details.Product','Details.Branch','Details.Warehouse')->where('re_order_level','>=','available_qty')->orWhereHas('Details',function ($detail){
//            $detail->whereDate('expire_date','>=', Carbon::now()->addMonth(6)->toDateString());
//        })->get());
    }
    public function allNotificationByDate(){
//        return Carbon::now()->addMonth(6);
        return Resource::collection(Product::with('Details','Details.Product','Details.Branch','Details.Warehouse')->whereHas('Details',function ($detail){
            $detail->whereDate('expire_date','<=', Carbon::now()->addMonth(6)->toDateString());
        })->get());
//        return Resource::collection(Product::with('Details','Details.Product','Details.Branch','Details.Warehouse')->where('re_order_level','>=','available_qty')->orWhereHas('Details',function ($detail){
//            $detail->whereDate('expire_date','>=', Carbon::now()->addMonth(6)->toDateString());
//        })->get());
    }
    public function getLots($product_id){
//        return new Resource(Product::with('Details')->find($product_id));
        return Resource::collection(ProductDetail::with('Product')->where('product_id',$product_id)->get());
    }

    public function openingStock(Request $request)
    {
        $results = json_decode($request->getContent(), true);
        foreach ($results as $result){
            if (empty($result['receive'])){
                continue;
//                $result['receive']=0;
            }
            $receive []=$result['receive'];
            /*
             *
             *
             * Update Purchase Detail table
             * Modify Total Receive Product
             *
             *
             * */
            if (isset($result['product'])){
                /*
                 *
                 *
                 * Update Product Available Stock, Actual Quantity, Expired Date
                 *
                 *
                 * */
                $product = Product::find($result['product']['id']);
                if (isset($result['mrp']))
                    $product->mrp = $result['mrp'];
                if (isset($result['expire_date']))
                    $product->expired_date = $result['expire_date'];
                $product->available_qty = $product->available_qty+$result['receive'];
                $product->actual_qty = $product->actual_qty+$result['receive'];
                $product->save();
                /*
                 *
                 * Insert Or Update Product Lot
                 *
                 * */
                $lot = ProductDetail::where('product_id',$product->id)
                    ->where('mrp',$result['product']['mrp'])
                    ->where('expire_date',$result['product']['expired_date'])
                    ->where('warehouse_id',$result['product']['warehouse_id'])
                    ->where('branch_id',$result['product']['branch_id'])
                    ->first();
                if (collect($lot)->isEmpty()){
                    $lot = new ProductDetail();
                }
                $lot->product_id = $product->id;
                $lot->qty = $lot->qty+$result['receive'];
                $lot->mrp = $result['mrp'];
                $lot->lot = kebab_case($product->name).'_'.$product->expired_date.'_'.$result['mrp'].'_'.$result['product']['branch_id'].'_'.$result['product']['warehouse_id'];
                $lot->branch_id = $result['product']['branch_id'];
                $lot->warehouse_id = $result['product']['warehouse_id'];
                $lot->expire_date = $result['product']['expired_date'];
                $lot->save();
            }
        }
        return $receive;
    }

    public function store(Request $request)
    {
        if ($request->action=="update"){
            $query = Product::find($request->id);
        }
        $query = new Product();
        $query->name = $request->name;
        if ($request->barcode !='undefined')
        $query->barcode = $request->barcode;
        if ($request->description !='undefined')
        $query->description = $request->description;
        if ($request->buying_price !='undefined')
        $query->buying_price = $request->buying_price;
        if ($request->selling_price !='undefined')
        $query->selling_price = $request->selling_price;
        if ($request->category_id !='undefined')
        $query->category_id = $request->category_id;
        if ($request->product_type_id !='undefined')
        $query->product_type_id = $request->product_type_id;
        if ($request->mrp !='undefined')
        $query->mrp = $request->mrp;
        if ($request->re_order_level !='undefined')
        $query->re_order_level = $request->re_order_level;
//        if ($request->expired_date !='undefined')
//        $query->expired_date = $request->expired_date;
        if ($request->re_order_level !='undefined')
        $query->re_order_level = $request->re_order_level;
        if ($request->unit_of_measure_id !='undefined')
        $query->unit_of_measure_id = $request->unit_of_measure_id;
        if ($request->branch_id !='undefined')
        $query->branch_id = $request->branch_id;
        if ($request->currency_id !='undefined')
        $query->currency_id = $request->currency_id;
        if ($request->warehouse_id !='undefined')
        $query->warehouse_id = $request->warehouse_id;
        $query->is_deleted = 0;
        $query->available_qty = 0;
        $query->actual_qty = 0;
        $query->save();
        return new Resource($query);
    }
    public function update(Request $request,$id)
    {
        $query = Product::find($id);
        $query->name = $request->name;
        if ($request->barcode !='undefined')
        $query->barcode = $request->barcode;
        if ($request->description !='undefined')
        $query->description = $request->description;
        if ($request->buying_price !='undefined')
        $query->buying_price = $request->buying_price;
        if ($request->selling_price !='undefined')
        $query->selling_price = $request->selling_price;
        if ($request->category_id !='undefined')
        $query->category_id = $request->category_id;
        if ($request->product_type_id !='undefined')
        $query->product_type_id = $request->product_type_id;
        if ($request->mrp !='undefined')
            $query->mrp = $request->mrp;
        if ($request->re_order_level !='undefined')
            $query->re_order_level = $request->re_order_level;
        if ($request->expired_date !='undefined')
//            $query->expired_date = $request->expired_date;
//        if ($request->re_order_level !='undefined')
            $query->re_order_level = $request->re_order_level;
        if ($request->unit_of_measure_id !='undefined')
        $query->unit_of_measure_id = $request->unit_of_measure_id;
        if ($request->branch_id !='undefined')
        $query->branch_id = $request->branch_id;
//        if ($request->currency_id !='undefined')
//        $query->currency_id = $request->currency_id;
        if ($request->warehouse_id !='undefined')
        $query->warehouse_id = $request->warehouse_id;
        $query->is_deleted = 0;
        $query->save();
        return new Resource($query);
    }

    public function updateLot(Request $request, $id){
        $lot = ProductDetail::find($id);
        if (isset($lot)){
            $lot->expire_date = $request->expire_date;
            $lot->save();
        }
    }
}
