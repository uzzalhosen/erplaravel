<?php

namespace App\Http\Controllers;

use App\Http\Resources\UnitOfMeasuereResource;
use App\UnitOfMeasure;
use Illuminate\Http\Request;

class UnitOfMeasureController extends Controller
{

    public function index()
    {
        return UnitOfMeasuereResource::collection(UnitOfMeasure::paginate(20));
    }


    public function store(Request $request)
    {
        if ($request->action=="update"){
            $query = UnitOfMeasure::find($request->id);
        }else{
            $query = new UnitOfMeasure();
        }
        $query->name = $request->name;
        $query->description = $request->description;
        $query->is_deleted = 0;
        $query->save();
        return $query;
    }

    public function update(Request $request,$id)
    {
        $query = UnitOfMeasure::find($id);
        $query->name = $request->name;
        $query->description = $request->description;
        $query->is_deleted = 0;
        $query->save();
        return $query;
    }
}
