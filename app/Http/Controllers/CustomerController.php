<?php

namespace App\Http\Controllers;

use App\customer;
use App\Http\Resources\CustomerResource;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        if ($request->page==0){

            return [
                "code" => "200",
                "status" => "success",
                "message" => "Item Fetched Successfully",
                "data" => [
                    "customer" => customer::with('Type')->get()
                ],
            ];
//            return CustomerResource::collection(customer::all());
        }else{
            if ($request->search != null){
                $customer = customer::with('Type')->where('name','like', '%' . $request->search . '%')->paginate(20);
            }else{
                $customer = customer::with('Type')->paginate(20);
            }

            return [
                "code" => "200",
                "status" => "success",
                "message" => "Item Fetched Successfully",
                "data" => [
                    "customer" => $customer
                ],
            ];
//            return CustomerResource::collection(customer::paginate(20));
        }
    }
//    public function CustomerList()
//    {
//        return CustomerResource::collection(customer::paginate(20));
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function store(Request $request)
    {
        $vendor = new customer();
        $vendor->name = $request->name;
        $vendor->address = $request->address;
        $vendor->city = $request->city;
        $vendor->state = $request->state;
        $vendor->zipcode = $request->zipcode;
        $vendor->phone = $request->phone;
        $vendor->email = $request->email;
        $vendor->contact_person = $request->contact_person;
        $vendor->customer_type_id = $request->customer_type_id;
        $vendor->giveDiscount = $request->giveDiscount;
        $vendor->discountBy = $request->discountBy;
        $vendor->discountAmount = $request->discountAmount;
        $vendor->save();

        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Saved Successfully",
            "data" => [
                "customer" => $vendor
            ],
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vendor = customer::find($id);
        $vendor->name = $request->name;
        $vendor->address = $request->address;
        $vendor->city = $request->city;
        $vendor->state = $request->state;
        $vendor->zipcode = $request->zipcode;
        $vendor->phone = $request->phone;
        $vendor->email = $request->email;
        $vendor->contact_person = $request->contact_person;
        $vendor->customer_type_id = $request->customer_type_id;
        $vendor->giveDiscount = $request->giveDiscount;
        $vendor->discountBy = $request->discountBy;
        $vendor->discountAmount = $request->discountAmount;
        $vendor->save();
        return $vendor;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(customer $customer)
    {
        //
    }
}
