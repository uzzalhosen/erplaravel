<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductTypeResource;
use App\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{

    public function index()
    {
        return ProductTypeResource::collection(ProductType::paginate(20));
    }


    public function store(Request $request)
    {
        if ($request->action=="update"){
            $query = ProductType::find($request->id);
        }else{
            $query = new ProductType();
        }
        $query->name = $request->name;
        $query->description = $request->description;
//        $query->is_deleted = 0;
        $query->save();
        return $query;
    }
    public function update(Request $request,$id)
    {
        $query = ProductType::find($id);
        $query->name = $request->name;
        $query->description = $request->description;
//        $query->is_deleted = 0;
        $query->save();
        return $query;
    }
}
