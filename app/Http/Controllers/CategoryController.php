<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        return CategoryResource::collection(Category::paginate(20));
    }


    public function store(Request $request)
    {
        if ($request->action=="update"){
            $category = Category::find($request->id);
        }else{
            $category = new Category();
        }
        $category->name = $request->name;
        $category->description = $request->description;
        $category->is_deleted = 0;
        $category->save();
        return $category;
    }

    public function update(Request $request,$id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->is_deleted = 0;
        $category->save();
        return $category;
    }


    public function inhdex()
    {
        return view('home');
    }


    public function indjex()
    {
        return view('home');
    }


    public function inddex()
    {
        return view('home');
    }
}
