<?php

namespace App\Http\Controllers;

use App\Http\Resources\DefaultResource;
use App\Product;
use App\Purchase;
use App\sale;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function test()
    {
        dd(Product::whereIn('id',array(1,2))->get()->sum('buying_price'));
    }
    public function dashboard()
    {
        $data = [];
        $dates = array(0);
        $salesArr = array(0);
        $purArr = array(0);
        $sales = sale::whereMonth('date', Carbon::now()->month)->where('status','Paid')->orWhere('status','Delivered')->orderByDesc('date')->get();
        $purchase = Purchase::whereMonth('date', Carbon::now()->month)->orderByDesc('date')->get();

        $start = Carbon::now()->startOfMonth()->format('Y-m');
        $end = Carbon::now()->endOfMonth()->format('d');

        $sDate = $sales->pluck('date')->toArray();
        $pDate = $purchase->pluck('date')->toArray();
//        $arDate = $this->concatArrays([$sDate, $pDate]);
        $arDate = $this->concatArrays([$sDate, $sDate]);
        if ($arDate){
            $maxDate =  date("d",max(array_map('strtotime',$arDate)));
            $minDate =  date("d",min(array_map('strtotime',$arDate)));

            for ($i=$minDate;$i<=$maxDate;$i++){
                if ($i<10){
//                $purchase[] = rand(0,550);
                }
//            $sales[] = rand(0,300);

                $date = $start.'-'.sprintf("%02d", $i);

                $sale = collect($sales)->where('date',$date)->first();

                if (collect($sale)->isNotEmpty()){
                    $salesArr[] = $sale->grand_total;
                    $purArr[] = $sale->buying_total;
                }else{
                    $salesArr[] = 0;
                    $purArr[] = 0;
                }

//                $pur = collect($purchase)->where('date',$date)->first();
//
//                if (collect($pur)->isNotEmpty()){
//                    $purArr[] = $pur->grand_total;
//                }else{
//                    $purArr[] = 0;
//                }


                $dates[] = $date;
            }
        }

        $total_sale = $sales->sum('grand_total');
//        $total_purchase = $purchase->sum('grand_total');
        $total_purchase = $sales->sum('buying_total');


        return [
            "data" => [
                "statistics" => [
                    "total_sale" => $total_sale,
                    "total_purchase" => $total_purchase,
                    "total_profit" => $total_sale - $total_purchase
                ],
                "recent_sales" => $sales->take(10),
                "graph" => [
                    "sale" => $salesArr,
                    "purchase" => $purArr,
//                    "allDates" => array_unique(array_merge($sDate, $pDate)),
                    "allDates" => $dates
                ],
//                "graph" => [
//                    "sale" => $sales->pluck('grand_total'),
//                    "purchase" => $purchase->pluck('grand_total'),
////                    "allDates" => array_unique(array_merge($sDate, $pDate)),
//                    "allDates" => $arDate
//                ]
            ],
        ];


    }
    public function concatArrays($arrays){
        $buf = [];
        foreach($arrays as $arr){
            foreach($arr as $v){
                $buf[$v] = true;
            }
        }
        return array_keys($buf);
    }
}
