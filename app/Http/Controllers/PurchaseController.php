<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Http\Resources\PurchaseResource;
use App\Http\Resources\Resource;
use App\Product;
use App\ProductDetail;
use App\Purchase;
use App\PurchaseDetail;
use App\sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchase = Purchase::with('Details.Product','Vendor','Branch')->orderByDesc('id')->paginate(20);

//        $purchase = Purchase::with('Details.Product','Vendor','Branch')->whereHas('Details', function($q){
//            $q->where('total_received', '!=','qty');
//        })->get();

        return Resource::collection($purchase)
            ->additional(['total' => [
                'amount' => $purchase->sum('cart_total'),
                'vat' =>  $purchase->sum('tax'),
                'discount' =>  $purchase->sum('discount'),
                'total' =>  $purchase->sum('grand_total'),
            ],
                'branches'=>Branch::all()
            ]);
    }
    public function filterByBranch($branch)
    {
        $purchase = Purchase::with('Details.Product','Vendor','Branch')->where('branch_id',$branch)->orderByDesc('id')->paginate(20);
        return Resource::collection($purchase)
            ->additional(['total' => [
                'amount' => $purchase->sum('cart_total'),
                'vat' =>  $purchase->sum('tax'),
                'discount' =>  $purchase->sum('discount'),
                'total' =>  $purchase->sum('grand_total'),
            ],
                'branches'=>Branch::all()
            ]);
    }
    public function getFilterBy($column,$data)
    {
        $sales = Purchase::with('Details.Product','Vendor','Branch')->where($column,$data)->orderByDesc('id')->paginate(20);
        return Resource::collection($sales)
            ->additional(['total' => [
                'amount' => $sales->sum('cart_total'),
                'vat' =>  $sales->sum('tax'),
                'discount' =>  $sales->sum('discount'),
                'total' =>  $sales->sum('grand_total'),
            ],
                'branches'=>Branch::all()
            ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function receivedProduct(Request $request)
    {
       $results = json_decode($request->getContent(), true);
        foreach ($results as $result){
            if (empty($result['receive'])){
                $result['receive']=0;
            }
            $receive []=$result['receive'];
            $detail = PurchaseDetail::find($result['id']);
            $detail->total_received = $detail->total_received+$result['receive'];
            if (isset($result['product'])){
                $product = Product::find($result['product']['id']);
                if (isset($result['product']['mrp']))
                $product->mrp = $result['product']['mrp'];
                if (isset($result['product']['expired_date']))
                $product->expired_date = $result['product']['expired_date'];
                $product->available_qty = $product->available_qty+$result['receive'];
                $product->save();
//                $receive [] = $result['product']['mrp'];
            }
            $detail->save();
        }
        return $receive;
    }

    public function LotReceive(Request $request)
    {
       $results = json_decode($request->getContent(), true);
        foreach ($results as $result){
            if (empty($result['receive'])){
                continue;
//                $result['receive']=0;
            }
            $receive []=$result['receive'];
            /*
             *
             *
             * Update Purchase Detail table
             * Modify Total Receive Product
             *
             *
             * */
            $detail = PurchaseDetail::find($result['id']);
            $detail->total_received = $detail->total_received+$result['receive'];
            if (isset($result['product'])){
                /*
                 *
                 *
                 * Update Product Available Stock, Actual Quantity
                 *
                 *
                 * */
                $product = Product::find($result['product']['id']);
                if (isset($result['product']['mrp']))
                $product->mrp = $result['product']['mrp'];
                if (isset($result['product']['expired_date']))
                $product->expired_date = $result['product']['expired_date'];
                $product->available_qty = $product->available_qty+$result['receive'];
                $product->actual_qty = $product->actual_qty+$result['receive'];
                $product->save();
                /*
                 *
                 * Insert Or Update Product Lot
                 *
                 * */
                $lot = ProductDetail::where('product_id',$product->id)
                    ->where('mrp',$result['product']['mrp'])
                    ->where('expire_date',$result['product']['expired_date'])
                    ->where('warehouse_id',$result['product']['warehouse_id'])
                    ->where('branch_id',$result['product']['branch_id'])
                    ->first();
                if (collect($lot)->isEmpty()){
                    $lot = new ProductDetail();
                }
                $lot->product_id = $result['product']['id'];
                $lot->qty = $lot->qty+$result['receive'];
                $lot->mrp = $result['product']['mrp'];
                $lot->lot = kebab_case($product->name).'_'.$result['product']['expired_date'].'_'.$result['product']['mrp'].'_'.$result['product']['branch_id'].'_'.$result['product']['warehouse_id'];
                $lot->branch_id = $result['product']['branch_id'];
                $lot->warehouse_id = $result['product']['warehouse_id'];
                $lot->expire_date = $result['product']['expired_date'];
                $lot->save();
            }
            $detail->save();
        }
        return $receive;
    }


    public function makePayment(Request $request)
    {
        $purchase                   =           Purchase::find($request->id);
        if ($purchase->due_amount=='0.00'){
//            return $sale->grand_total-$request->amount;
            $purchase->due_amount       =           $purchase->grand_total-$request->amount;
        }else{
//            return 'not 0.00';
            $purchase->due_amount       =           $purchase->due_amount-$request->amount;
        }
//        if ($purchase->grand_total===($purchase->due_amount+$request->amount)){
//            $purchase->status ='Paid';
//        }

        if ($purchase->grand_total<=($purchase->paid_amount+$request->amount)){
//            return $sale->paid_amount;
            $purchase->status ='Paid';
        }else{
//            return 'grant total '.$sale->grand_total;
            $purchase->status ='Partial Paid';
        }
        $purchase->paid_amount      =           $purchase->paid_amount+$request->amount;
        $purchase->save();


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result= $request->getContent();
//        return (array)$request->products;
//        $result = json_decode($result);
//        return $request->products;
//        dd($request->all());
//        return redirect($_SERVER['HTTP_REFERER']);
//        dd($request->all()) ;
        $purchase = new Purchase();
        $purchase->vendor_id=$request->vendor_id;
        $purchase->date=date('Y-m-d');
        $purchase->status='Pending';
        $purchase->salesman_id=$request->salesman_id;
        $purchase->branch_id=$request->branch_id;
        $purchase->cart_total=$request->subtotal;
        if (isset($request->YourReference)){

            $purchase->YourReference=$request->YourReference;
        }
        else{

            $purchase->YourReference='---';
        }
        $purchase->OurReference=$request->OurReference;
        $purchase->discount=$request->discount;
        $purchase->discount_type=$request->discount_type;
        $purchase->tax=0;
        $purchase->vat='0';
        $purchase->grand_total=$request->grand_total;
        $purchase->paid_amount='0';
        $purchase->due_amount='0';
        $purchase->save();
        foreach ($request->products as $item=>$value){
            $purchaseDetail = new PurchaseDetail();
            $purchaseDetail->purchase_id = $purchase->id;
            $purchaseDetail->product_id = $request->products[$item]['product_id'];
            $purchaseDetail->qty = $request->products[$item]['quantity'];
            $purchaseDetail->total_received = 0;
            $purchaseDetail->return_qty = 0;
            $purchaseDetail->unit_price = $request->products[$item]['price'];
            $purchaseDetail->sub_total = $request->products[$item]['amount'];
            $purchaseDetail->save();
//            $purchaseDetail = new PurchaseDetail();
//            $purchaseDetail->purchase_id = $purchase->id;
//            $purchaseDetail->product_id = $request->products[$item];
//            $purchaseDetail->qty = $request->quantity[$item];
//            $purchaseDetail->total_received = 0;
//            $purchaseDetail->return_qty = 0;
//            $purchaseDetail->unit_price = $request->price[$item];
//            $purchaseDetail->sub_total = $request->amount[$item];
//            $purchaseDetail->save();
        }
//        return $arr;
//        return redirect($_SERVER['HTTP_REFERER']);
//        $purchaseDetail = new PurchaseDetail();
//        $purchaseDetail->purchase_id = $purchase->id;
//        $purchaseDetail->product_id = ;

//        $products = explode(',',$request->products);
//        foreach ($products as $product){
//            $data = $product;
//        }
//        $decoded = json_decode($request->products);
//        return $decoded[0]->selectedProduct;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new Resource(Purchase::with('Vendor','Details','Details.Product','Branch')->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }
}
