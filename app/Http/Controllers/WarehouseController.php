<?php

namespace App\Http\Controllers;

use App\BranchWarehouse;
use App\Http\Resources\WarehouseResource;
use App\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{

    public function index()
    {
        return WarehouseResource::collection(Warehouse::paginate(20));
    }


    public function store(Request $request)
    {
//        return $request->branch_id;
        if ($request->action=="update"){
            $query = Warehouse::find($request->id);
        }else{
            $query = new Warehouse();
        }
        $query->name = $request->name;
        $query->description = $request->description;
        $query->phone = $request->phone;
        $query->mobile = $request->mobile;
        $query->address_1 = $request->address_1;
        $query->address_2 = $request->address_2;
        $query->city = $request->city;
        $query->state = $request->state;
        $query->pin = $request->pin;
        $query->is_deleted = 0;
        $query->save();
//        $query->Branches()->attach($request->branch_id);

        if ($request->branch_id){
            $branches = explode(',',$request->branch_id);
            foreach ( $branches as $item=>$value){
                $branchWarehouse = new BranchWarehouse();
                $branchWarehouse->warehouse_id = $query->id;
                $branchWarehouse->branch_id = $value;
                $branchWarehouse->save();
            }
        }
        return $query;
    }

    public function update(Request $request,$id)
    {
        return $request->branch_id;

        $query = Warehouse::find($id);
        $query->name = $request->name;
        $query->description = $request->description;
        $query->phone = $request->phone;
        $query->mobile = $request->mobile;
        $query->address_1 = $request->address_1;
        $query->address_2 = $request->address_2;
        $query->city = $request->city;
        $query->state = $request->state;
        $query->pin = $request->pin;
        $query->is_deleted = 0;
        $query->save();

        if ($request->branch_id){
            $branches = explode(',',$request->branch_id);
            foreach ( $branches as $item=>$value){
                $branchWarehouse = new BranchWarehouse();
                $branchWarehouse->warehouse_id = $query->id;
                $branchWarehouse->branch_id = $value;
                $branchWarehouse->save();
            }
        }
        return $query;
    }
}
