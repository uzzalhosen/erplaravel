<?php

namespace App\Http\Controllers;

use App\Http\Resources\SaleDetailResource;
use App\saleDetail;
use Illuminate\Http\Request;

class SaleDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SaleDetailResource::collection(saleDetail::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function delivered()
    {
        return SaleDetailResource::collection(saleDetail::where('total_received','!=',0)->paginate(20));
    }

    public function getFilterBy(Request $request)
    {

        $saleDetails = saleDetail::with('Product','Sale')->whereHas('Sale', function($q) use ($request){
            $q->whereBetween('date',[$request->filterDate['start_date'],$request->filterDate['end_date']]);
        })->orderByDesc('id')->paginate(20);

        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Fetched Successfully",
            "data" => [
                "saleDetails" => $saleDetails
            ],
        ];
    }

    public function searchProductLedgerByDate($from,$to)
    {

        $saleDetails = saleDetail::with('Product','Sale','Sale.Customer')->whereHas('Sale', function($q) use ($from, $to){
            $q->whereBetween('date',[$from,$to])->whereIn('status',['Paid','Delivered']);
        })->orderByDesc('id')->paginate(20);
        $summary = saleDetail::with('Product','Sale','Sale.Customer')->whereHas('Sale', function($q) use ($from, $to){
            $q->whereBetween('date',[$from,$to])->whereIn('status',['Paid','Delivered']);
        })->orderByDesc('id')->get();
        return [
            "code" => "200",
            "status" => "success",
            "message" => "Item Fetched Successfully",
            "data" => [
                "saleDetails" => $saleDetails,
                "summary"=>[
                    "total"=> $summary->sum('qty')
                ]
            ],
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\saleDetail  $saleDetail
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        return new SaleDetailResource(saleDetail::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\saleDetail  $saleDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(saleDetail $saleDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\saleDetail  $saleDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, saleDetail $saleDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\saleDetail  $saleDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(saleDetail $saleDetail)
    {
        //
    }
}
