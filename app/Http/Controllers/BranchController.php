<?php

namespace App\Http\Controllers;

use App\Branch;
use App\BranchWarehouse;
use App\Http\Resources\BranchResource;
use App\Http\Resources\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BranchController extends Controller
{

    public function index()
    {
//        return DB::select('select * from branches where 1');
        return BranchResource::collection(Branch::paginate(20));
    }
    public function all()
    {
//        return DB::select('select * from branches where 1');
        return BranchResource::collection(Branch::all());
    }

    public function getWarehouseByBranch($branch)
    {
//        return DB::select('select * from branches where 1');
        $branch = Branch::find($branch);
        return Resource::collection($branch->Warehouse);
    }


    public function store(Request $request)
    {
        if ($request->action=="update"){
            $query = Branch::find($request->id);
        }else{
            $query = new Branch();
        }
        $query->name = $request->name;
        $query->description = $request->description;
        $query->address = $request->address;
        $query->city = $request->city;
        $query->state = $request->state;
        $query->zipcode = $request->zipcode;
        $query->phone = $request->phone;
        $query->email = $request->email;
        $query->contact_person = $request->contact_person;
        $query->is_deleted = 0;
        $query->save();
        return $query;
    }

    public function update(Request $request, $id)
    {

        $query = Branch::find($id);
        $query->name = $request->name;
        $query->description = $request->description;
        $query->address = $request->address;
        $query->city = $request->city;
        $query->state = $request->state;
        $query->zipcode = $request->zipcode;
        $query->phone = $request->phone;
        $query->email = $request->email;
        $query->contact_person = $request->contact_person;
        $query->is_deleted = 0;
        $query->save();
        return $query;
    }
    public function filtered(Request $request){
        $branchs = BranchWarehouse::where('warehouse_id',$request->warehouse_id)->with('Branches')->get();
        return \App\Http\Resources\BranchWarehouse::collection($branchs);
    }
    public function filter(){
        $branchs = BranchWarehouse::where('warehouse_id',1)->with('Branches')->get();
//        foreach ($branchs as $branch){
//            echo $branch->Branches;
//        }
        return \App\Http\Resources\BranchWarehouse::collection($branchs);
//        return response()->json($branchs->Branches);
    }
}
