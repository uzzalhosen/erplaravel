<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Http\Resources\CurrencyResource;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{

    public function index()
    {
        return CurrencyResource::collection(Currency::all());
    }


    public function store(Request $request)
    {
        if ($request->action=="update"){
            $query = Currency::find($request->id);
        }else{
            $query = new Currency();
        }
        $query->name = $request->name;
        $query->description = $request->description;
        $query->is_deleted = 0;
        $query->save();
        return $query;
    }
}
