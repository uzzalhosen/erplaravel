<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'customer'=>$this->Customer,
            'OurReference'=>$this->OurReference,
            'YourReference'=>$this->YourReference,
            'status'=>$this->status,
            'cart_total'=>$this->cart_total,
            'discount'=>$this->discount,
            'tax'=>$this->tax,
            'vat'=>$this->tax,
            'grand_total'=>$this->grand_total,
            'paid_amount'=>$this->paid_amount,
            'due_amount'=>$this->due_amount,
            'date'=>$this->created_at->format('Y-m-d'),
            'detail'=>$this->Details,
        ];
    }
}
