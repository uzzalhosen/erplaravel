<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'qty'=>$this->qty,
            'total_received'=>$this->total_received,
            'return_qty'=>$this->return_qty,
            'unit_price'=>$this->unit_price,
            'sub_total'=>$this->sub_total,
            'type'=>$this->type,
            'sale'=>$this->Sale,
            'customer'=>$this->Sale?$this->Sale->Customer:null,
            'product'=>$this->Product,
            'date'=>$this->created_at->format('Y-m-d'),
        ];
    }
}
