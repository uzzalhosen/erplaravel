<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WarehouseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'description'=>$this->description,
            'phone'=>$this->phone,
            'mobile'=>$this->mobile,
            'address_1'=>$this->address_1,
            'address_2'=>$this->address_2,
            'city'=>$this->city,
            'state'=>$this->state,
            'pin'=>$this->pin,
            'is_deleted'=>$this->is_deleted,
            'branches'=>$this->Branches,
        ];
    }
}
