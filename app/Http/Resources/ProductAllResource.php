<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductAllResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id'=>$this->id,
            'name'=>$this->name,
            'barcode'=>$this->barcode,
            'description'=>$this->description,
            'productDescription'=>$this->description,
            'buying_price'=>$this->buying_price,
            'total_buying_price'=>$this->buying_price*$this->available_qty,
            'selling_price'=>$this->selling_price,
            'mrp'=>$this->mrp,
            'expired_date'=>$this->expired_date,
            're_order_level'=>$this->re_order_level,
            'available_qty'=>$this->available_qty,
            'actual_qty'=>$this->actual_qty,
            'product_type'=>$this->ProductType,
            'product_type_id'=>$this->product_type_id,
            'category'=>$this->Category,
            'unit_of_measure'=>$this->UnitOfMeasure,
            'unit_of_measure_id'=>$this->unit_of_measure_id,
            'branch'=>$this->Branch,
            'branch_id'=>$this->branch_id,
            'currency'=>$this->Currency,
            'currency_id'=>$this->currency_id,
            'warehouse'=>$this->Warehouse,
            'warehouse_id'=>$this->warehouse_id,
            'details'=>$this->Details
        ];
    }
}
