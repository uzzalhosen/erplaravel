<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'vendor'=>$this->Vendor,
            'salesman_id'=>$this->salesman_id,
            'branch_id'=>$this->branch_id,
            'status'=>$this->status,
            'cart_total'=>$this->cart_total,
            'discount'=>$this->discount,
            'tax'=>$this->tax,
            'vat'=>$this->tax,
            'grand_total'=>$this->grand_total,
            'paid_amount'=>$this->paid_amount,
            'due_amount'=>$this->due_amount,
            'date'=>$this->created_at->format('Y-m-d'),
        ];
    }
}
