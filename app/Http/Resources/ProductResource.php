<?php

namespace App\Http\Resources;

use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use mysql_xdevapi\Collection;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'current_page' => $this->currentPage(),
            'first_page_url' => $this->url($this->firstItem()),
            'next_page_url' => $this->nextPageUrl(),
            'last_page' => $this->lastPage(),
            'last_page_url' => $this->url($this->lastPage()),
            'total' => $this->total(),
            'per_page' => $this->perPage(),
            'prev_page_url' => $this->previousPageUrl(),
            'count' => $this->count(),
            'product' =>collect($this->items())->map(function ($product){
                return [
                    'id'=>$product->id,
                    'name'=>$product->name,
                    'barcode'=>$product->barcode,
                    'description'=>$product->description,
                    'productDescription'=>$product->description,
                    'buying_price'=>$product->buying_price,
                    'total_buying_price'=>$product->buying_price*$product->available_qty,
                    'selling_price'=>$product->selling_price,
                    'mrp'=>$product->mrp,
                    'expired_date'=>$product->expired_date,
                    're_order_level'=>$product->re_order_level,
                    'available_qty'=>$product->available_qty,
                    'actual_qty'=>$product->actual_qty,
                    'product_type'=>$product->ProductType,
                    'product_type_id'=>$product->product_type_id,
                    'category'=>$product->Category,
                    'unit_of_measure'=>$product->UnitOfMeasure,
                    'unit_of_measure_id'=>$product->unit_of_measure_id,
                    'branch'=>$product->Branch,
                    'branch_id'=>$product->branch_id,
                    'currency'=>$product->Currency,
                    'currency_id'=>$product->currency_id,
                    'warehouse'=>$product->Warehouse,
                    'warehouse_id'=>$product->warehouse_id,
                    'details'=>$product->Details,
                ];
            })
        ];
    }
}
