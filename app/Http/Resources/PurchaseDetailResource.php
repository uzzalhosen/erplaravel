<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'qty'=>$this->qty,
            'total_received'=>$this->total_received,
            'return_qty'=>$this->return_qty,
            'unit_price'=>$this->unit_price,
            'sub_total'=>$this->sub_total,
            'type'=>$this->type,
            'purchase'=>$this->Purchase,
            'vendor'=>$this->Purchase->Vendor,
            'product'=>$this->Product,
            'date'=>$this->created_at->format('Y-m-d'),
        ];
    }
}
