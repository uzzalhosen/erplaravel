<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VendorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'address'=>$this->address,
            'city'=>$this->city,
            'state'=>$this->state,
            'zipcode'=>$this->zipcode,
            'phone'=>$this->phone,
            'email'=>$this->email,
            'contact_person'=>$this->contact_person,
            'vendor_type_id'=>$this->vendor_type_id,
            'type'=>$this->Type,
        ];
    }
}
