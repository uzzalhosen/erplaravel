<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'address'=>$this->address,
            'city'=>$this->city,
            'state'=>$this->state,
            'zipcode'=>$this->zipcode,
            'phone'=>$this->phone,
            'email'=>$this->email,
            'contact_person'=>$this->contact_person,
            'customer_type_id'=>$this->customer_type_id,
            'type'=>$this->Type,
            'giveDiscount'=>$this->giveDiscount,
            'discountBy'=>$this->discountBy,
            'discountAmount'=>$this->discountAmount,
        ];
    }
}
