<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'branches'=>parent::toArray($request),
            'id'=>$this->id,
            'name'=>$this->name,
            'description'=>$this->description,
            'address'=>$this->address,
            'city'=>$this->city,
            'state'=>$this->state,
            'zipcode'=>$this->zipcode,
            'phone'=>$this->phone,
            'email'=>$this->email,
            'contact_person'=>$this->contact_person,
            'is_deleted'=>$this->is_deleted
        ];
//        return parent::toArray($request);
    }
}
