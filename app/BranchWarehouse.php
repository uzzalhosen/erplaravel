<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchWarehouse extends Model
{
    public function Warehouse(){
        return $this->belongsToMany(Warehouse::class,'branch_warehouses','warehouse_id','branch_id');
    }
    public function Branch(){
        return $this->belongsToMany(Branch::class,'branch_warehouses','branch_id','warehouse_id');
    }
    public function Branches(){
        return $this->belongsTo(Branch::class,'branch_id');
    }
}
