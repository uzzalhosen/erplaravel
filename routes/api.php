<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('sale', 'SaleController');
Route::resource('sale-detail', 'SaleDetailController');
Route::resource('purchase', 'PurchaseController');
Route::resource('purchase-detail', 'PurchaseDetailController');
Route::resource('branch', 'BranchController');
Route::resource('category', 'CategoryController');
Route::resource('currency', 'CurrencyController');
Route::resource('product', 'ProductController');
Route::resource('product-type', 'ProductTypeController');
Route::resource('unit-of-measure', 'UnitOfMeasureController');
Route::resource('warehouse', 'WarehouseController');
Route::resource('vendor', 'VendorController');
Route::resource('vendor-type', 'VendorTypeController');
Route::resource('customer', 'CustomerController');
Route::resource('customer-type', 'CustomerTypeController');

Route::post('/branch/filtered', 'BranchController@filtered')->name('branch.filtered');
Route::get('branches', 'BranchController@all')->name('branch.index');
Route::get('branch/warehouse/{id}', 'BranchController@getWarehouseByBranch')->name('branch.getWarehouseByBranch');
Route::get('filter', 'BranchController@filter')->name('branch.filter');
Route::get('received-product', 'PurchaseDetailController@received')->name('purchase.detail.received');
Route::post('receive-product', 'PurchaseController@receivedProduct')->name('purchase.receive');
Route::post('lot-receive', 'PurchaseController@LotReceive')->name('lot.receive');
Route::post('receive-payment', 'SaleController@receive')->name('sale.receive');
Route::post('total-stock', 'SaleController@TotalStock')->name('sale.receive');
Route::post('make-payment', 'PurchaseController@makePayment')->name('purchase.makePayment');
Route::post('deliver-product', 'SaleController@deliverProducts')->name('sale.deliverProducts');
Route::post('change-action', 'SaleController@changeAction');
Route::get('sales/delete/{id}', 'SaleController@deleteSale');
Route::get('delivered-product', 'SaleDetailController@delivered')->name('sale.detail.delivered');
Route::get('sales/filter/{branch}', 'SaleController@filterByBranch')->name('sale.filterByBranch');
//Route::get('sales/filter/{column}/{data}/{filterDate}/{isDue}', 'SaleController@getFilterBy')->name('sale.getFilterBy');
Route::post('sales/filter', 'SaleController@getFilterBy')->name('sale.getFilterBy');
Route::get('sales/filter-by-date/{from}/{to}/{isDue}', 'SaleController@searchSalesByDate')->name('sale.searchSalesByDate');

Route::get('product-ledger/filter-by-date/{from}/{to}', 'SaleDetailController@searchProductLedgerByDate');

Route::get('sales/filter-by-date-due/{from}/{to}', 'SaleController@searchSalesByDateDue');
Route::get('all-sales', 'SaleController@getAllSales')->name('sale.getAllSales');
Route::get('customer-list', 'CustomerController@CustomerList');

Route::get('purchase/filter/{branch}', 'PurchaseController@filterByBranch')->name('purchase.filterByBranch');
Route::get('purchase/filter/{column}/{data}', 'PurchaseController@getFilterBy')->name('purchase.getFilterBy');

//Route::post('add-delivery-date/', 'SaleController@getFilterBy')->name('purchase.getFilterBy');


/**
 *
 *
 * Sales Man Report Generate
 * 
 *
 */
Route::post('salesman-report/generate', 'SaleController@salesManReport')->name('salesman.report');
Route::get('due-sales', 'SaleController@dueSales')->name('sales.due');


/*
 * Notification
 *
 * */
Route::get('get-list-n-by-qty', 'ProductController@allNotificationByQty');
Route::get('get-list-n-by-date', 'ProductController@allNotificationByDate');


Route::get('getLots/{product_id}', 'ProductController@getLots')->name('product.lots');
Route::post('openingStock', 'ProductController@openingStock')->name('product.openingStock');

/*
 *
 * Dashboard
 *
 * */

Route::get('dashboard', 'HomeController@dashboard')->name('erp.dashboard');

/*
 *
 * Lot
 *
 * */

Route::put('lot/{id}', 'ProductController@updateLot');
