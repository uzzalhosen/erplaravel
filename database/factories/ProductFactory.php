<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        "name"=>$faker->colorName,
        "barcode"=>$faker->isbn13,
        "description"=>$faker->paragraph,
        "buying_price"=>rand(20,80),
        "selling_price"=>rand(30,90),
        "category_id"=>function() {
            return factory('App\Category')->create()->id;
        },
        "product_type_id"=>function() {
            return factory('App\ProductType')->create()->id;
        },
        "unit_of_measure_id"=>function() {
            return factory('App\UnitOfMeasure')->create()->id;
        },
        "branch_id"=>function() {
            return factory('App\Branch')->create()->id;
        },
        "currency_id"=>function() {
            return factory('App\Currency')->create()->id;
        },
        "warehouse_id"=>function() {
            return factory('App\Warehouse')->create()->id;
        },
    ];
});
