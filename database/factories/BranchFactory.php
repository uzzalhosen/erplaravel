<?php

use Faker\Generator as Faker;

$factory->define(App\Branch::class, function (Faker $faker) {
    return [
        'name'=> $faker->colorName,
        'description'=> $faker->paragraph(3),
        'address'=> $faker->address,
        'city'=> $faker->city,
        'state'=> $faker->city,
        'zipcode'=> $faker->postcode,
        'phone'=> $faker->phoneNumber,
        'email'=> $faker->email,
        'contact_person'=> $faker->name,
    ];
});
