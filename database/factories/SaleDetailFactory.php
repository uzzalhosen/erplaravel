<?php

use Faker\Generator as Faker;

$factory->define(App\saleDetail::class, function (Faker $faker) {
    return [
        'sale_id'=>rand(1,5),
        'product_id'=>rand(1,5),
        'qty'=>rand(1,5),
        'total_received'=>rand(0,5),
        'return_qty'=>rand(0,2),
        'unit_price'=>rand(1,5),
        'sub_total'=>rand(1,5),
        'type'=>'purchase',
    ];
});
