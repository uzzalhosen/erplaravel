<?php

use Faker\Generator as Faker;

$factory->define(App\sale::class, function (Faker $faker) {
    return [
        'customer_id'=>rand(1,5),
        'date'=>$faker->date(),
        'status'=>$faker->randomElement($array = array ('Pending','partial')),
        'cart_total'=>rand(1,8),
        'discount'=>'0.00',
        'tax'=>'0.00',
        'vat'=>'0.00',
        'grand_total'=>'0.00',
        'paid_amount'=>'0.00',
        'due_amount'=>'0.00',
    ];
});
