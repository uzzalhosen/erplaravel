<?php

use Faker\Generator as Faker;

$factory->define(App\ProductType::class, function (Faker $faker) {
    return [
        "name"=>$faker->colorName,
        "description"=>$faker->paragraph,
    ];
});
