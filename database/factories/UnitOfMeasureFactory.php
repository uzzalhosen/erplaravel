<?php

use Faker\Generator as Faker;

$factory->define(App\UnitOfMeasure::class, function (Faker $faker) {
    return [
        "name"=>$faker->randomElement($array = array ('KG','L','ML','P')),
        "description"=>$faker->paragraph,
    ];
});
