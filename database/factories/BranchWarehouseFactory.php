<?php

use Faker\Generator as Faker;

$factory->define(App\BranchWarehouse::class, function (Faker $faker) {
    return [
        "warehouse_id"=>rand(1,10),
        "branch_id"=>rand(1,10),
    ];
});
