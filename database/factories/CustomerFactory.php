<?php

use Faker\Generator as Faker;

$factory->define(App\customer::class, function (Faker $faker) {
    return [
        "name"=>$faker->name,
        "address"=>$faker->address,
        "city"=>$faker->city,
        "state"=>$faker->country,
        "zipcode"=>$faker->postcode,
        "phone"=>$faker->phoneNumber,
        "email"=>$faker->email,
        "contact_person"=>$faker->firstName,
        "customer_type_id"=>function() {
            return factory('App\customerType')->create()->id;
        },
    ];
});
