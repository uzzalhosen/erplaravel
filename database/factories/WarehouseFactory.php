<?php

use Faker\Generator as Faker;

$factory->define(App\Warehouse::class, function (Faker $faker) {
    return [
        "name"=>$faker->colorName,
        "description"=>$faker->sentence(3),
        "phone"=>$faker->phoneNumber,
        "mobile"=>$faker->phoneNumber,
        "address_1"=>$faker->address,
        "address_2"=>$faker->address,
        "city"=>$faker->city,
        "state"=>$faker->city,
        "pin"=>rand(999,9999),
    ];
});
