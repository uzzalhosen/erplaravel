<?php

use Faker\Generator as Faker;

$factory->define(App\Vendor::class, function (Faker $faker) {
    return [
        "name"=>$faker->name,
        "address"=>$faker->address,
        "city"=>$faker->city,
        "state"=>$faker->country,
        "zipcode"=>$faker->postcode,
        "phone"=>$faker->phoneNumber,
        "email"=>$faker->email,
        "contact_person"=>$faker->firstName,
        "vendor_type_id"=>function() {
            return factory('App\VendorType')->create()->id;
        },
    ];
});
