<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('salesman_id')->unsigned();
            $table->date('date');
            $table->string('status');
            $table->string('YourReference')->nullable();
            $table->string('OurReference')->nullable();
            $table->integer('branch_id')->unsigned()->nullable();
            $table->float('cart_total',10,2);
            $table->float('discount_amount',10,2)->nullable();
            $table->float('discount',10,2)->nullable();
            $table->string('discount_type')->default('fixed');
            $table->float('tax',10,2)->nullable();
            $table->float('tax_amount',10,2)->nullable();
            $table->float('vat',10,2)->nullable();
            $table->float('vat_amount',10,2)->nullable();
            $table->float('grand_total',10,2);
            $table->float('buying_total',10,2);
            $table->float('paid_amount',10,2)->nullable();
            $table->float('due_amount',10,2)->nullable();
            $table->timestamps();
            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
