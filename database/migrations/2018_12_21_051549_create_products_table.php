<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('barcode')->nullable();
            $table->text('description')->nullable();
            $table->float('buying_price',10,2)->default(0); //cost
            $table->float('selling_price',10,2)->default(0); //tp
            $table->float('mrp',10,2)->default(0); // MRP
            $table->integer('product_type_id')->unsigned()->nullable();
            $table->date('expired_date')->nullable();
            $table->integer('re_order_level')->nullable();
            $table->integer('available_qty')->nullable();
            $table->integer('actual_qty')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('unit_of_measure_id')->unsigned()->nullable();
            $table->integer('branch_id')->unsigned()->nullable();
            $table->integer('currency_id')->unsigned()->nullable();
            $table->integer('warehouse_id')->unsigned()->nullable();
            $table->boolean('is_deleted')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('product_type_id')->references('id')->on('product_types');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('unit_of_measure_id')->references('id')->on('unit_of_measures');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('warehouse_id')->references('id')->on('warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
