<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->unsigned();
            $table->integer('salesman_id')->unsigned()->nullable();
            $table->integer('branch_id')->unsigned()->nullable();
            $table->date('date');
            $table->string('YourReference');
            $table->string('OurReference');
            $table->string('status');
            $table->float('cart_total',10,2);
            $table->float('discount_amount',10,2)->nullable();
            $table->float('discount',10,2)->nullable();
            $table->string('discount_type')->default('fixed');
            $table->float('tax',10,2)->nullable();
            $table->float('tax_amount',10,2)->nullable();
            $table->float('vat',10,2)->nullable();
            $table->float('vat_amount',10,2)->nullable();
            $table->float('grand_total',10,2);
            $table->float('paid_amount',10,2)->nullable();
            $table->float('due_amount',10,2)->nullable();
            $table->timestamps();
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
