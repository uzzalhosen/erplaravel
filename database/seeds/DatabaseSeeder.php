<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

//        factory(App\Category::class, 50)->create();
//        factory(App\ProductType::class, 50)->create();
//        factory(App\UnitOfMeasure::class, 50)->create();
//        factory(App\Branch::class, 50)->create();
//        factory(App\Currency::class, 50)->create();
//        factory(App\Warehouse::class, 50)->create();
//        factory(App\Warehouse::class, 50)->create()->each(function ($warehouse){
//            $warehouse->Branches()->save(factory(App\Branch::class)->make());
//        });
        factory(App\Product::class, 10)->create()->each(function ($product) {
//            $product->Category()->save(factory(App\Category::class)->make());
//            $product->ProductType()->save(factory(App\ProductType::class)->make());
//            $product->UnitOfMeasure()->save(factory(App\UnitOfMeasure::class)->make());
//            $product->Branch()->save(factory(App\Branch::class)->make());
//            $product->Currency()->save(factory(App\Currency::class)->make());
//            $product->Warehouse()->save(factory(App\Warehouse::class)->make());
        });

        factory(App\BranchWarehouse::class, 15)->create();
        factory(App\Vendor::class, 15)->create();
        factory(App\customer::class, 15)->create();
        factory(App\Purchase::class, 15)->create();
        factory(App\PurchaseDetail::class, 15)->create();
        factory(App\sale::class, 15)->create();
        factory(App\saleDetail::class, 15)->create();
    }
}
